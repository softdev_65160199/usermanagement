/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.mycompany.usermanamentproject;

import java.awt.List;
import java.util.ArrayList;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author sarit
 */
public class testUserservice {
    UserService userService;
    public testUserservice() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
        userService = new UserService();
        User newAdmin = new User("admin", "Administrator", "pass1234", 'M','A');
        User newUser1 = new User("user1", "User1", "pass@1234", 'M','U');
        User newUser2 = new User("user2", "User2","pass_1234",'F','U');
        User newUser3 = new User("user3", "User3","pass#1234",'F','U');
        User newUser4 = new User("user4", "User4","pass#1234",'M','U');
        userService.addUser(newAdmin);
        userService.addUser(newUser1);
        userService.addUser(newUser2);
        userService.addUser(newUser3);
        userService.addUser(newUser4);
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void testAddUser() {
        System.out.println("addUser");
        User newUser = new User("admin", "Administrator", "pass1234", 'M','A');
        UserService instance = new UserService();
        User expResult = newUser;
        User result = instance.addUser(newUser);
        assertEquals(expResult,result);
    }
    
    @Test
    public void testGetUser() {
        System.out.println("getUser");
        int index = 1;
        
        String expResult = "user1";
        User result = userService.getUser(index);
        assertEquals(expResult,result.getLogin());
    }
    
    @Test
    public void testGetUsers() {
        System.out.println("getUsers");
        ArrayList<User> userList = userService.getUsers();
        assertEquals(5,userList.size());
    }
    @Test
    public void testGetSize() {
        System.out.println("getSize");
        int expResult = 5;
        int result = userService.getSize();
        assertEquals(expResult,result);
    }
}

